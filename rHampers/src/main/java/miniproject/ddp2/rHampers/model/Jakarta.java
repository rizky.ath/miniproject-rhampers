package miniproject.ddp2.rHampers.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Jakarta {
	private String daerah;
	private List<Toko> listToko;
	
	public Jakarta(String daerah) {
		this.daerah = daerah;
		this.listToko = new ArrayList<Toko>();
	}

	public String getDaerah() {
		return daerah;
	}

	public void setDaerah(String daerah) {
		this.daerah = daerah;
	}

	public List<Toko> getListToko() {
		return listToko;
	}

	public void setListToko(List<Toko> listToko) {
		this.listToko = listToko;
	}
	
	public void addToko(Toko toko) {
		this.listToko.add(toko);
	}
	
	public abstract String toString();
	
}
