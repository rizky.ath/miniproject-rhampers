package miniproject.ddp2.rHampers.model;

import java.util.ArrayList;
import java.util.List;

//KAYANYA GADIPAKE, SIMULATORNYA PAKE YANG SPRINGBOOT AJA DI CLASS MainController.
public class MainConsole {
	private static List<Jakarta> daerahJakarta = new ArrayList<Jakarta>();
	
	public MainConsole() {
		
	}
	
	public static Jakarta getDaerah(String daerah) {
		for (Jakarta x : daerahJakarta) {
			if (x.getDaerah().equalsIgnoreCase(daerah)) {
				return x;
			}
		}
		return null;
	}
	
	public int getTokoSeluruhJakarta() {
		int jumlahToko = 0;
		for (Jakarta x : daerahJakarta) {
			jumlahToko += x.getListToko().size();
		}
		return jumlahToko;
	}
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JakartaTimur jakartaTimur = new JakartaTimur();
		daerahJakarta.add(jakartaTimur);
		JakartaBarat jakartaBarat = new JakartaBarat();
		daerahJakarta.add(jakartaBarat);
		JakartaPusat jakartaPusat = new JakartaPusat();
		daerahJakarta.add(jakartaPusat);
		JakartaSelatan jakartaSelatan = new JakartaSelatan();
		daerahJakarta.add(jakartaSelatan);
		JakartaUtara jakartaUtara = new JakartaUtara();
		daerahJakarta.add(jakartaUtara);
		KepulauanSeribu kepulauanSeribu = new KepulauanSeribu();
		daerahJakarta.add(kepulauanSeribu);
	}

}
