package miniproject.ddp2.rHampers.model;

import java.util.List;
import java.util.ArrayList;

public class Toko {
	private String nama;
	private List<Hampers> listHampers;
	private String alamat;
	private String gambar;
	
	public Toko(String nama, String alamat) {
		this.nama = nama;
		this.alamat = alamat;
		this.listHampers = new ArrayList<Hampers>();
	}
	
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public List<Hampers> getListHampers() {
		return listHampers;
	}
	
	public void addListHampers(Hampers hampers) {
		listHampers.add(hampers);
	}
	
	public String getAlamat() {
		return alamat;
	}
	
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	public void insertGambar(String file) {
		this.gambar = file;
	}
	
	public String getFileGambar() {
		return this.gambar;
	}
	
	
}
