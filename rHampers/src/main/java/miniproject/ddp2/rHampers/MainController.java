package miniproject.ddp2.rHampers;

import miniproject.ddp2.rHampers.model.*;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("user", "User");
		return "index";
	}
	
	@GetMapping("/pilih-daerah")
	public String pilihDaerah(@RequestParam(value = "namaDaerah", required=false) String namaJakarta,
								Model model) {
		model.addAttribute("jakarta", MainConsole.getDaerah(namaJakarta));
		return "pilih-daerah";
		
	}
	
	@PostMapping("/pilih-daerah")
	public String listTokoDaerah(@ModelAttribute Jakarta daerahJakarta) {
		return "hasil-pilih-daerah";
	}
	

}
