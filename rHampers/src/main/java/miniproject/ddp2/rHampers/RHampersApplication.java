package miniproject.ddp2.rHampers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RHampersApplication {

	public static void main(String[] args) {
		SpringApplication.run(RHampersApplication.class, args);
	}

}
